Various random useful Python hacks
====

Starting with remote debugging, running PDB over telnet. Useful if you have
a Python process buried somewhere in a server farm.
